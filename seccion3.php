<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Lista de servidores web</title>
</head>
<body>
    <h1>Lista de servidores web</h1>
    <div>
    <p>Apache<a href="https://httpd.apache.org/docs/2.4/es/">  https://httpd.apache.org/docs/2.4/es/ </a></p>
</div>
    <div>

    <p>Servidor Nginx<a href="https://kinsta.com/es/base-de-conocimiento/que-es-nginx/">  https://kinsta.com/es/base-de-conocimiento/que-es-nginx/  </a></p>
    </div>  

    <div>

    <p>LiteSpeed<a href="https://www.litespeedtech.com/">  https://www.litespeedtech.com/  </a></p>
    </div>  

    <div>

    <p>Servidor Microsoft IIS<a href="https://blog.infranetworking.com/servidor-iis/">  https://blog.infranetworking.com/servidor-iis/  </a></p>
    </div>

    <div>

    <p>Sun Java System Web Server<a href="https://www.martechforum.com/herramienta/sun-java-system-web-server/">  https://www.martechforum.com/herramienta/sun-java-system-web-server/  </a></p>
    </div>
    
</body>
</html>