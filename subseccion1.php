<html> 
    <head>
        <title>Fracmentos de libros</title>
       <!-- <link rel="shortcut" type="image/x-icon" href="imagenes/favicon.ico">-->
        <meta name="description" content= "Pasamos mucho tiempo sin darnos cuenta de que estamos vivos, 
            corriendo y buscando sin parar las respuestas a las preguntas que no nos motivamos a formular."/>
        <meta charset="utf-8"/>
        <link rel="shortcut icon" href="favicon.png">
        
    </head>
    <body>
        <h1>Carta del rio</h1>


        <p>
            Pasamos mucho tiempo sin darnos cuenta de que estamos vivos, corriendo y buscando sin parar las 
            respuestas a las <b>preguntas que no nos motivamos</b> a <strong>formular.</strong> No importa 
            quién soy, no importa si soy de este planeta o vivo en otro <span>tipo de energía</span>. Te veo
            y pienso: ¡Estás disperso! Tan alejado de ti, tan huyendo en tus miedos creyendo que estás 
            <i>dejándolos atrás</i>. Te escondes detrás de tus tristezas en vez de agradecer tu breve existencia.
            Te escondes en tus problemas teniendo la vida manifestándose. Te escondes mientras crees que estás
            buscando el amor. Buscas desesperadamente amar para llenar tus vacíos. Te llenas en soledad, te llenas
            cuando decides observar en el espejo y decidir quién quieres ser.</p>
            <a href="https://archive.org/stream/AmorACuatroEstacionesNacaridPortalArraez/Amor%20a%20cuatro%20estaciones%20-%20Nacarid%20Portal%20Arraez_djvu.txt">Amor a cuatro AmorACuatroEstacionesNacaridPortalArraez</a>

        
    </body>
     <html>