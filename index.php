<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Taller metadatos</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <header>
        <nav class="navegacion">
            <ul class="menu">
                <li><a href="#">SECCION 1</a>
                <ul class="submenu"> 
                <li><a href="subseccion1.php">AMOR A CUATRO ESTACIONES</a></li>
                <li><a href="subseccion2.php">EL PERFUME</a></li>
                <li><a href="subseccion3.php">ONCE MINUTOS</a></li> 
                </ul>
                </li>
                <li><a href="seccion2.php">SECCION 2</a></li>
                <li><a href="seccion3.php">SECCION 3</a></li>    
            </ul>
        </nav>
    </header>
</body>
</html>