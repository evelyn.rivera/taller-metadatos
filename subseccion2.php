<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content= "Grenouille crece en un hogar para niños huérfanos hasta los 
            ocho años bajo el cuidado de madame Gaillard."/>
    <title>seccion 2</title>
</head>
<body>
   <h1>El perfume</h1>

        <p>
            <strong>Grenouille</strong> crece en un hogar para niños huérfanos hasta los ocho años bajo el cuidado 
            de madame Gaillard.Ella lo entrega a un cruel curtidor de cueros que pone a <strong>Grenouille</strong>
            a cargo de los trabajos más peligrosos.
            Una noche, durante la celebración anual en honor a la posesión del rey, Grenoille percibe un aroma perfecto. 
            Lo sigue hasta la rue de Marais donde encuentra a su primera víctima: <i> una joven adolecente pelirroja</i> 
            excepcionalmente bella, que se encuentra quitando las semillas de unas ciruelas amarillas. Grenouille 
            <span>la asesina para poder poseer su aroma</span>.

            El protagonista, consciente de su talento único, decide trabajar en perfumería y llama la atención del perfumista 
            más prestigioso de París, Baldini. Bajo Baldini, <strong>Grenouille</strong> aprende los procesos químicos para 
            destilar y materializar perfumes.
        
        </p>
            <a href="https://www.culturagenial.com/es/novela-el-perfume-de-patrick-suskind/">El perfume Patrick Süskin</a>
    
</body>
</html>