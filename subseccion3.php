<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta name="description" content= "Maria es de un pueblo situado al norte de Brasil. Todavía adolescente, viaja a Río de Janeiro, donde conoce a
                un empresario que le ofrece un buen trabajo en Ginebra."/>
</head>
<body>
    <h1>Once minutos</h1>
    <p>
        Maria es de un pueblo situado al norte de Brasil. Todavía adolescente, viaja a Río de Janeiro, donde conoce a
        un empresario que le ofrece un buen trabajo en Ginebra. Allí, Maria sueña con encontrar fama y fortuna, pero 
        acabará ejerciendo la prostitución. El aprendizaje que extraerá de sus duras experiencias modificará para siempre 
        su actitud ante sí misma y ante la vida.

        Como un cuento de hadas para adultos, Once minutos es una novela que explora la naturaleza del sexo y del
        amor, la intensa y difícil relación entre cuerpo y alma, y cómo alcanzar la perfecta unión entre ambos. Once
        minutos ofrece al lector una experiencia inigualable de lectura y reflexión.</p>
        <a href="https://www.planetadelibros.com/libro-once-minutos/262800#:~:text=Como%20un%20cuento%20de%20hadas,inigualable%20de%20lectura%20y%20reflexi%C3%B3n.">Once minutos Paulo Coelho</a>
</body>
</html>
